import config from './config.js';
import jwt from 'jsonwebtoken';

const JWT_KEY = config.get("jwt_key");
const USER_KEY = "taw_user_token";

export function check_auth_middleware(req, res, next) {
    req.current_user = null;
    if (!req.cookies[USER_KEY]) 
    {
        res.status(401).send("Error while checking access token");
    }
    else
    {
        jwt.verify(req.cookies[USER_KEY], JWT_KEY, function (err, user) 
        {
            if (err) {
                res.status(401).send("Error while checking access token");
            }
            else {
                req.current_user = user;
                next();
            }
        });
    }
}

export function check_auth(req) {
    var current_user = null;
    if (req.cookies[USER_KEY]) 
    {
        current_user = jwt.verify(req.cookies[USER_KEY], JWT_KEY);
    }
    return current_user;
}

export function generate_cookie_token(res, key, data)
{
    const token = jwt.sign(data, JWT_KEY);

    res.cookie(key, token, {httpOnly:true, secure:true, sameSite: 'strict'});
}

export function generate_user_cookie_token(res, data)
{
    generate_cookie_token(res, USER_KEY, data)
}
