import fs from 'fs';

const configPath = './config.json';
var config = JSON.parse(fs.readFileSync(configPath, 'UTF-8'));

const env = process.env.taw_environment ?? "dev";
config = config[env] ?? config["dev"];
config.get = function (key, default_value="") {
    var result = this;
    key.split("/").every((subKey) => {
        result = result[subKey];
        if (typeof result == 'undefined')
        {
            result = default_value;
            return false;
        } 
        return true;
    });
    return result;
}
config.getAllowList = function () {
    return this.get("allowList", []).map((url) => ["http://" + url, "https://" + url]).flat();
}

export default config;