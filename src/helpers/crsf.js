import debug from 'debug';
import config from './config.js';
import { doubleCsrf } from "csrf-csrf";

function getTokenFromRequest(req) {
    return req.headers["x-xsrf-token"];
}

export const {
    invalidCsrfTokenError, // This is just for convenience if you plan on making your own middleware.
    generateToken, // Use this in your routes to provide a CSRF hash + token cookie and token.
    validateRequest, // Also a convenience if you plan on making your own middleware.
    doubleCsrfProtection, // This is the default CSRF protection middleware.
} = doubleCsrf({
    getSecret: (req) => config.get("csrf-key", "123456789-591-PEAR-pen-abcdefghi"), // A function that optionally takes the request and returns a secret
    ignoredMethods: ["GET", "HEAD", "OPTIONS"],
    getTokenFromRequest: getTokenFromRequest // A function that returns the token from the request
});
