import pgp from 'pg-promise'/* options */
import config from './config.js'

var db = pgp()(
    `postgres://${config.db.username}:${config.db.password}@${config.db.host}:${config.db.port}/${config.db.database}`
)

export default db;