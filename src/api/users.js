import express from 'express';
import { generate_user_cookie_token, check_auth_middleware, check_auth } from '../helpers/jwt.js';
import bcrypt from 'bcrypt';
import db from "../helpers/database.js";

// salt round to define processing time order for hashing
const saltRounds = 8;


var router = express.Router();

/* GET users listing. */
// Login route
router.post('/login', async function(req, res, next) {
  const password = req.body.password;
  const email = req.body.email;
  var result = {status: "ERROR", message: "Credentials are invalid"};

  try {
    var user = await db.oneOrNone(
      "SELECT id, name, email, company_id, pwd FROM taw_user WHERE email=$1 LIMIT 1",
      [email]
    );
  }
  catch(e){
    return next(e);
  }
 
  if (user) {
    var ok = await bcrypt.compare(password, user.pwd);
    if (ok)
    {
      user = {id: user.id, name: user.name,  company_id: user.company_id };
      generate_user_cookie_token(res, user);
      result = {status: "VALID", message: "", data: user};
    }
  }
  res.json(result);
});

// Logout route
router.post('/logout', (req, res) => {
  res.json({status: "VALID", message: ""});
})

// Logout route
router.get('/is_logged', async (req, res) => {
  var current_user = check_auth(req);
  res.json(current_user !== null && "id" in current_user);
})

router.get('/user_data', check_auth_middleware, async (req, res) => {
  res.json(req.current_user);
})

// Logout route
router.post('/signup', async function(req, res, next) {
  const email = req.body.email;
  const name = req.body.name;
  const surname = req.body.surname ?? "";
  const company_id = req.body.company_id ?? 1;
  var result = {status: "ERROR", message: "unknown"};

  try {
    const existing = await db.oneOrNone(
      "SELECT email FROM taw_user WHERE email=$1 LIMIT 1",
      [email]
    );
    if (existing)
    {
       result["message"] = "This email is already used";
    }
    else
    {
      const hashedPassword = await bcrypt.hash(req.body.password, saltRounds);
      await db.none(
        "INSERT INTO taw_user(name, surname, email, pwd, company_id) VALUES($1, $2, $3, $4, $5)",
        [name, surname, email, hashedPassword, company_id]
      );
      result["status"] = "VALID";
    }
  }
  catch(e){
    return next(e);
  }
  res.json(result);
});

export default router;
