import express from 'express';
import { check_auth_middleware } from '../helpers/jwt.js';
var router = express.Router();
import db from "../helpers/database.js";

var router = express.Router();

router.get('/wastes', check_auth_middleware, async (req, res, next) => {
    var result = {status: "VALID", message: "", data: 0};
  
    try {
      var wastes = await db.many(
        `select DATE_TRUNC('month', waste.date) as date_month,
          waste_type.name as type_name, SUM(waste.units) as total_units
        from waste
          left join waste_type on waste.type=waste_type.id
          inner join taw_user on waste.company_id=taw_user.company_id
        where taw_user.id=$1
        GROUP BY date_month, waste_type.name`,
        [req.current_user.id]
      );
    }
    catch(e){
      return next(e);
    }
    var wastes_by_type = wastes.reduce((accumulator, current) => {
      var type_name = current["type_name"];
      if (!accumulator[type_name])
        accumulator[type_name] = [];
      accumulator[type_name].push(current);
      return accumulator;
    }, {});
    result.data = wastes_by_type;
    res.json(result);
});

export default router;
