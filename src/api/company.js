import express from 'express';
var router = express.Router();
import db from "../helpers/database.js";

var router = express.Router();

router.get('/all_names', async (req, res, next) => {  
    var companies = [];

    try {
      const filter = req.query.filter ?? "";
      const limit = req.query.limit ?? -1;

      var request = "select name from COMPANY";

      if (filter)
      {
        request += " where name like %$1%"; 
      }

      if (limit && limit > 0)
      {
        request += " limit $2"; 
      }

      companies = (await db.many(
        request, [filter, limit]
      )).map(x => x.name);
    }
    catch(e) {
      return next(e);
    }

    res.json(companies);
})

export default router;