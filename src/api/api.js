import express from 'express';
import message from './message.js'
import users from './users.js'
import company from './company.js'
import wastes from './wastes.js'
var router = express.Router();

/* GET home page. */
router.use('/message', message);
router.use('/users', users);
router.use('/company', company);
router.use('/wastes', wastes);
router.get('/crsf_end_point', (req, res) =>
{
    const crsf_key = "XSRF-TOKEN";
    res.cookie(crsf_key, req.csrfToken(), {httpOnly:false, secure:false, sameSite: 'strict'});
    res.send(true);
});

export default router;
