import express from 'express';
var router = express.Router();

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/* GET users listing. */
router.get('/', async (req, res) => {
    await sleep(2000);
    res.json("Coucou")
});

export default router;
