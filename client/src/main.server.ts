import { bootstrapApplication } from '@angular/platform-browser';
import { TawMainComponent } from './app/main/app.component';
import { config } from './app/app.config.server';

const bootstrap = () => bootstrapApplication(TawMainComponent, config);

export default bootstrap;
