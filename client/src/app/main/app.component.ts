import { Component, afterRender } from '@angular/core';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { WastesService } from '../services/wastes.service';
import { TawHelperComponent } from '../helpers/taw_helper.component';
import { CommonModule } from '@angular/common';
import { TawNavComponent } from '../nav/nav.component';

@Component({
  selector: 'taw-main',
  standalone: true,
  imports: [
    RouterOutlet, RouterLink, RouterLinkActive, 
    CommonModule, TawNavComponent, TawHelperComponent
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})

export class TawMainComponent {
  title = 'client';
  data = {};

  constructor(private apiService: WastesService) { }

  ngOnInit() { }
}
