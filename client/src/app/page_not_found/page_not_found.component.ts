import { Component } from '@angular/core';

@Component({
  selector: 'app-page_not_found',
  standalone: true,
  imports: [],
  templateUrl: './page_not_found.component.html',
  styleUrl: './page_not_found.component.scss'
})
export class TawPageNotFoundComponent {

}
