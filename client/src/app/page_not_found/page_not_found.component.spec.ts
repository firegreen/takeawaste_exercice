import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TawPageNotFoundComponent } from './page_not_found.component';

describe('PageNotFoundComponent', () => {
  let component: TawPageNotFoundComponent;
  let fixture: ComponentFixture<TawPageNotFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TawPageNotFoundComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TawPageNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
