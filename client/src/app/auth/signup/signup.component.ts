import { AfterViewInit, Component, ElementRef } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { UserResult, BaseUserService } from '../../services/user.service';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';

const password_pattern = Validators.pattern(
  /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&^_.-]).{8,}/
)

@Component({
  selector: 'app-signup',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule],
  templateUrl: './signup.component.html',
  styleUrl: './signup.component.scss'
})
export class SignupComponent implements AfterViewInit {
  password: FormControl;
  name: FormControl;
  email: FormControl;
  confirmedPassword: FormControl;
  form: FormGroup;

  submitted = false;
  result: UserResult = new UserResult();
  returnUrl = "";

  constructor(
    private formBuilder: FormBuilder, private userService: BaseUserService,
    private currentRoute: ActivatedRoute, private router: Router
  ) 
  {
    this.password = new FormControl(
      '', Validators.compose([Validators.required, password_pattern])
    )
    this.name = new FormControl('', Validators.required)
    this.email = new FormControl(
      '', Validators.compose([Validators.required, Validators.email])
    )
    this.confirmedPassword = new FormControl('')
    this.form = this.formBuilder.group(
      {
        "name": this.name,
        "email": this.email,
        "password": this.password,
        "confirmed_password": this.confirmedPassword
      }, 
      {
        updateOn: "blur",
        validators: [this.matchPassword], 
        asyncValidators: null
      }
    );
   
  }
  
  ngAfterViewInit(): void {
    Object.entries(this.form.controls).forEach(entry => {
      const [key, control] = entry;
      control.setValue("");
    });
  }

  check_errors(formControl: AbstractControl, key="")
  {
    formControl.updateValueAndValidity();
    var result = formControl.errors && (this.submitted || formControl.value);
    if (key)
      result = result && formControl.errors != null && formControl.errors[key];
    return result;
  }

  ngOnInit() {
    this.currentRoute.queryParams.subscribe(params => {
      this.returnUrl = params["returnUrl"] ?? "";
    });
  }

  getFormValidationErrors() {
    var result: {[key: string]: any} = {};

    Object.entries(this.form.controls).forEach(entry => {
      const [key, control] = entry;
      control.updateValueAndValidity();
      const controlErrors: ValidationErrors|null = control.errors;
      if (controlErrors != null) {
        Object.entries(controlErrors).forEach(errorEntry => {
          const [keyError, error] = errorEntry;
          const fullKey = `${key}.${keyError}`;
          result[fullKey] = error;
        });
      }
    });
    return result;
  }

  matchPassword(form: FormGroup): ValidationErrors | null {
      if (form.get("password")?.value != form.get("confirmedPassword")?.value)
        return {"matching": "Passwords do not match."};
      return null;
  }

  onSubmit()
  {
    this.submitted = true;

    if (Object.keys(this.getFormValidationErrors()).length)
    {
      return;
    }

    this.userService.signup(
      this.name.value, this.email.value, this.password.value
    ).subscribe(data => {
      this.result = data as UserResult;

      if (this.result.requestSucceed())
      {
        if (this.returnUrl)
          this.router.navigate(["login"], {queryParams: {"returnUrl": this.returnUrl}});
        else
          this.router.navigate(["login"]);
      } 
    });
  }
}
