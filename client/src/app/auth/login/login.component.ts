import { AfterViewInit, Component, Inject } from '@angular/core'
import { FormControl, FormGroup, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { UserResult, BaseUserService } from '../../services/user.service';
import { CommonModule, DOCUMENT } from '@angular/common';
import { ActivatedRoute, Router, RouterLink, RouterLinkActive, RouterStateSnapshot } from '@angular/router';
import { error } from 'node:console';
import { NotAuthentifiedComponent } from '../../helpers/authentification_check';

const password_pattern = Validators.pattern(
  /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&^_-]).{8,}/
)


@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule, RouterLink, RouterLinkActive],
  host: {ngSkipHydration: 'true'},
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent extends NotAuthentifiedComponent implements AfterViewInit {

  email: AbstractControl;
  password: AbstractControl;
  form: FormGroup;

  submitted = false;
  result = new UserResult();
  returnUrl = "";

  constructor(
    private currentRoute: ActivatedRoute,
    @Inject(DOCUMENT) private document: Document
  ) 
  {
    super();
    this.email = new FormControl(
      '', Validators.compose([Validators.required, Validators.email])
    );
    this.password = new FormControl(
      ''
    );

    this.form = new FormGroup(
      {
        email: this.email,
        password: this.password
      }, 
      {
        updateOn: "blur",
        validators: null, 
        asyncValidators: null
      }
    )
  }

  check_errors(formControl: AbstractControl, key="")
  {
    if (!formControl)
      return []
    // formControl.updateValueAndValidity();
    var result = formControl.errors && (this.submitted || formControl.value);
    if (key)
      result = result && formControl.errors != null && formControl.errors[key];
    return result;
  }

  override async ngOnInit() {
    await super.ngOnInit()
    this.currentRoute.queryParams.subscribe(params => {
      this.returnUrl = params["returnUrl"] ?? "";
    });
  }

  ngAfterViewInit(): void {
    var inputs = this.document.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
      inputs.item(i)!.removeAttribute("readonly");
    }
  }


  getFormValidationErrors() {
    var result: {[key: string]: any} = {};

    Object.entries(this.form.controls).forEach(entry => {
      const [key, control] = entry;
      // control.updateValueAndValidity();
      const controlErrors: ValidationErrors|null = control.errors;
      if (controlErrors != null) {
        Object.entries(controlErrors).forEach(errorEntry => {
          const [keyError, error] = errorEntry;
          const fullKey = `${key}.${keyError}`;
          result[fullKey] = error;
        });
      }
    });
    return result;
  }

  onSubmit()
  {
    this.submitted = true;
    var inputs = this.document.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
      inputs.item(i)!.focus();
    }
    inputs.item(0)!.focus();

    if (!Object.keys(this.getFormValidationErrors()).length)
    {
      var request = this.userService.login(
        this.email.value, 
        this.password.value
      )

      request.subscribe(data => {
        this.result = data;   
        if (this.result.requestSucceed())
        {
          console.log(this.returnUrl);
          if (this.returnUrl)
            this.router.navigateByUrl(this.returnUrl);
          else
            this.router.navigate(["dashboard"]);
        } 
        else 
        {

        }  
      })
    }
  }
}


