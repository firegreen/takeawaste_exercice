import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TawNavComponent } from './nav.component';

describe('NavComponent', () => {
  let component: TawNavComponent;
  let fixture: ComponentFixture<TawNavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TawNavComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TawNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
