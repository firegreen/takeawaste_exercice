import { Inject, Injectable, Optional, PLATFORM_ID, inject } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HTTP_INTERCEPTORS, HttpInterceptorFn, HttpHandlerFn, HttpXsrfTokenExtractor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { isPlatformServer } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';

@Injectable({providedIn: "root"})
class HttpRequestInterceptor {

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private tokenExtractor: HttpXsrfTokenExtractor,
    private cookieService: CookieService,
    @Optional() @Inject("CLIENT_REQ") private request: any
  ) {}

  transfer_client_cookie(req: HttpRequest<any>)
  {
    if (isPlatformServer(this.platformId) && this.request && this.request.headers) {
      req = req.clone({ headers: req.headers.set("cookie", this.request.headers.get("cookie") ?? "") });
    }
    return req;
  }

  add_xsrf_header(req: HttpRequest<any>)
  {
    const cookieheaderName = 'X-XSRF-TOKEN';
    let csrfToken = this.tokenExtractor.getToken() as string;

    if (csrfToken !== null && !req.headers.has(cookieheaderName)) {
        req = req.clone({ headers: req.headers.set(cookieheaderName, csrfToken) });
    }
    return req;
  }
  
  intercept(req: HttpRequest<any>, next: HttpHandlerFn): Observable<HttpEvent<any>> {    
    req = this.transfer_client_cookie(req);
    req = this.add_xsrf_header(req);
    return next(req);
  }
}

export const httpIntercept: HttpInterceptorFn = (
    req: HttpRequest<any>,
    next: HttpHandlerFn
): Observable<HttpEvent<any>> => 
{
    return inject(HttpRequestInterceptor).intercept(req, next);
}