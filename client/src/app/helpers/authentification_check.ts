import { Component, Injectable, inject } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { BaseUserService } from '../services/user.service';
import { firstValueFrom } from 'rxjs';
import { OnInit } from "@angular/core";

@Injectable({providedIn: "root"})
class AuthentifiedCheck {

    constructor(private router: Router, private authService: BaseUserService) { }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        var connected = await firstValueFrom(this.authService.isLogged());
        if (connected !== null && !connected)
        {
            // not logged in so redirect to login page with the return url
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
            return false;
        }

        return true;
    }

}

@Injectable({providedIn: "root"})
class NotAuthentifiedCheck {

    constructor(private router: Router, private authService: BaseUserService) { }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        var connected = await firstValueFrom(this.authService.isLogged());
        if (connected !== null && connected)
        {
            // not logged in so redirect to login page with the return url
            this.router.navigate(['/dashboard'], { queryParams: { returnUrl: state.url }});
            return false;
        }
        return true;
    }

}

export async function check_authentified(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
{
    await inject(AuthentifiedCheck).canActivate(route, state);
}

export async function check_not_authentified(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
{
    await inject(NotAuthentifiedCheck).canActivate(route, state);
}

@Component({
    selector: "authentified-component",
    template: `<p>Not Implemented!</p>`
})
export class AuthentifiedComponent implements OnInit {

    protected router = inject(Router);
    protected userService = inject(BaseUserService);

    constructor() {}

    async ngOnInit(): Promise<void> {
        var connected = await firstValueFrom(this.userService.isLogged());
        if (connected !== null && !connected)
        {
            this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url }});
        }
    }
}

@Component({
    selector: "not-authentified-component",
    template: `<p>Not Implemented!</p>`
})
export class NotAuthentifiedComponent implements OnInit {

    protected router: Router = inject(Router);
    protected userService: BaseUserService = inject(BaseUserService);

    constructor() {}

    async ngOnInit(): Promise<void> {
        var connected = await firstValueFrom(this.userService.isLogged());
        if (connected !== null && connected)
        {
            this.router.navigate(['/dashboard'], { queryParams: { returnUrl: this.router.url }});
        }
    }
}