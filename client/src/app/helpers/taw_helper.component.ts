import { isPlatformBrowser } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AfterViewInit, Inject, Injectable, OnInit, Optional, PLATFORM_ID, inject } from '@angular/core';
import { Component } from '@angular/core';
import { Observable, firstValueFrom, map } from 'rxjs';

const CRSF_URL = 'https://localhost:3000/api/crsf_end_point';
const httpAuthOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Cache-Control': 'no-cache'}),
  withCredentials: true, // this is required so that Angular returns the Cookies received from the server. 
  // The server sends cookies in Set-Cookie header.
  // Without this, Angular will ignore the Set-Cookie header
};

@Component({
  selector: 'taw-helper',
  standalone: true,
  imports: [],
  template: '<div></div>',
})

export class TawHelperComponent implements OnInit {
  constructor(@Inject(PLATFORM_ID) private platformId: Object, protected http: HttpClient) { }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.http.get(CRSF_URL, httpAuthOptions).subscribe(
        result => {}
      );  // add subscribe, otherwise the request is not sent
    }
  }
}
