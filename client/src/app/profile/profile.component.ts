import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef } from '@angular/core';
import {
   AbstractControl, AbstractControlOptions, FormBuilder, 
   FormControl, FormGroup, ValidationErrors, Validators 
} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { UserResult, BaseUserService } from '../services/user.service';
import { CommonModule } from '@angular/common';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { MatCommonModule } from '@angular/material/core';
import { CompanyService } from '../services/company.service';
import { AuthentifiedComponent } from '../helpers/authentification_check';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

const phone_pattern = Validators.pattern(
  /(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})/
)

@Component({
  selector: 'app-profile',
  standalone: true,
  imports: [
    ReactiveFormsModule, CommonModule, 
    MatCommonModule, MatFormFieldModule, MatInputModule, 
    MatButtonModule, 
    MatAutocompleteModule, MatIconModule
  ],
  templateUrl: './profile.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrl: './profile.component.scss'
})
export class ProfileComponent extends AuthentifiedComponent implements AfterViewInit {
  name: FormControl;
  company: FormControl;
  phone: FormControl;
  adress: FormControl;
  form: FormGroup;

  companies_filter: Array<string> = [];

  submitted = false;
  result: UserResult = new UserResult();
  returnUrl = "";
  errors: {[key: string]: string[]}

  constructor(
    private formBuilder: FormBuilder, private companyService: CompanyService,
    private currentRoute: ActivatedRoute
  ) 
  {
    super();
    this.name = new FormControl('', { validators: Validators.required, updateOn: "blur" });
    this.company = new FormControl('', { validators: Validators.required, updateOn: "change" });
    this.phone = new FormControl('', phone_pattern);
    this.adress = new FormControl('');
    this.errors = {};

    this.form = this.formBuilder.group(
      {
        "name": this.name,
        "company": this.company,
        "phone": this.phone,
        "adress": this.adress,
      }, 
      {
        updateOn: "submit",
        validators: [], 
        asyncValidators: null
      } as AbstractControlOptions
    );

    Object.entries(this.form.controls).forEach(entry => {
      const [key, control] = entry;
      this.errors[key] = [];
    });
  }

  update_companies_filter() {
    this.companyService.get_all_companies_request(this.company.value).subscribe(
      (result) => this.companies_filter = result
    )
  }
  
  ngAfterViewInit(): void {
    Object.entries(this.form.controls).forEach(entry => {
      let [key, control] = entry;
      control.setValue("");
      control.statusChanges.subscribe(
        status => {
          this.check_errors_of_control(key);
        } 
      )
      control.updateValueAndValidity();
    });
    console.log(Object.keys(this.form.controls).length);
    this.company.valueChanges.subscribe(
      value => {
        this.update_companies_filter()
      }
    );
  }

  check_errors_of_control(control_name: string)
  {
    var form_control = this.form.controls[control_name];
    this.errors[control_name] = [];

    const control_errors: ValidationErrors|null = form_control.errors;

    if (control_errors != null) {
      Object.entries(control_errors).forEach(errorEntry => {
        const [keyError, error] = errorEntry;
        if (error)
        {
          this.errors[control_name].push(keyError);
        }

      });
    }
    
    return this.errors[control_name];
  }

  getFormValidationErrors() {
    Object.entries(this.form.controls).forEach(entry => {
      const [key, control] = entry;
      this.check_errors_of_control(key);
    });
    return this.errors;
  }

  onSubmit()
  {
    this.submitted = true;

    if (Object.keys(this.getFormValidationErrors()).length)
    {
      return;
    }

    // this.userService.profile(
    //   this.name.value, this.email.value, this.password.value
    // ).subscribe(data => {
    //   this.result = data as UserResult;

    //   if (this.result.requestSucceed())
    //   {
    //     if (this.returnUrl)
    //       this.router.navigate(["login"], {queryParams: {"returnUrl": this.returnUrl}});
    //     else
    //       this.router.navigate(["login"]);
    //   } 
    // });
  }
}
