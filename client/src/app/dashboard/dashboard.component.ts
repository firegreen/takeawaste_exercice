import { AfterViewInit, Component, ElementRef, Inject, PLATFORM_ID, Type} from '@angular/core';
import { AuthentifiedComponent } from '../helpers/authentification_check';
import { CommonModule, isPlatformBrowser, isPlatformServer } from '@angular/common';
import { NgxEchartsDirective, provideEcharts } from 'ngx-echarts';
import { WastesService } from '../services/wastes.service';
import * as echarts from 'echarts';


@Component({
  selector: 'empty',
  standalone: true,
  host: {ngSkipHydration: 'true'},
  template: "",
})
class EmptyComponent { }


@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [CommonModule],
  host: {ngSkipHydration: 'true'},
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss',
  providers: [
    provideEcharts(),
  ]
})
export class DashboardComponent extends AuthentifiedComponent implements AfterViewInit {
  chartOption: echarts.EChartsOption = {
    title: {
      text: 'Total wastes over time'
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: '#6a7985'
        }
      }
    },
    xAxis: {
      type: 'time'
    },
    yAxis: {
      type: 'value',
    },
    series: [],
  };
  chart!: echarts.EChartsType;
  isLoading: boolean = false;
  // echarts_component: Type<any>;
  // echarts_inputs: Record<string, any>;
  
  constructor(
    private wastes_service: WastesService, @Inject(PLATFORM_ID) private platformId: Object,
    private rootElement: ElementRef
  ) { 
    super();
    // this.echarts_component = EmptyComponent;
    // this.echarts_inputs = {};
  }

  ngAfterViewInit(): void {
    if (isPlatformServer(this.platformId)) {
      return;
    }
    const charts = (<HTMLElement>this.rootElement.nativeElement).querySelectorAll('.taw-chart');
    charts.forEach(element => {
      this.chart = echarts.init(
        element as HTMLDivElement
      );
      this.chart.setOption(this.chartOption);
      this.wastes_service.wastes_request().subscribe(
        result => {
          var data: {[key: string]: Array<any>} = result.data ?? [];
          var series: Array<echarts.LineSeriesOption> = [];
          Object.entries(data).forEach((entry) =>
            {
              var [type, wastes] = entry;
              series.push({
                data: wastes.map(waste_row => [waste_row["date_month"], waste_row["total_units"]]),
                type: 'line',
                name: type,
                smooth: true,
                stack: 'Total',
                areaStyle: {},
                emphasis: {
                  focus: 'series'
                },
              })
            }
          )
          this.chart.setOption(
            {
              series: series.sort(
                (a, b) => {
                  const a_data: Array<any> = a.data ? a.data : [];
                  const b_data: Array<any> = b.data ? b.data : [];
                  return b_data.reduce((total, current) => total + current[1], 0) - 
                  a_data.reduce((total, current) => total + current[1], 0);
                }
              )
            }
          )
        }
      );
    });
  }

  // onChartInit(chart)
  // {
  //   this.chart = chart;
  //   this.wastes_service.wastes_request().subscribe(
  //     result => {
  //       this.chart.setOption(
  //         {
  //           series: [{
  //             data: [new Date().getTime(), result],
  //             type: 'line',
  //           }]
  //         }
  //       )
  //     }
  //   )
  // }
}
function TitleOption(arg0: string): echarts.TitleComponentOption | echarts.TitleComponentOption[] | undefined {
  throw new Error('Function not implemented.');
}

