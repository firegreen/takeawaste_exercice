import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { provideClientHydration } from '@angular/platform-browser';
import { provideHttpClient, withFetch, withInterceptors, withXsrfConfiguration } from '@angular/common/http';
import { httpIntercept } from './helpers/http_interceptor';
import { userServiceProvider } from './services/user.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { wasteServiceProvider } from './services/wastes.service';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { companyServiceProvider } from './services/company.service';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';


export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes), provideClientHydration(), 
    provideHttpClient(
      withFetch(), withInterceptors([httpIntercept]),
       withXsrfConfiguration({cookieName: "XSRF-TOKEN", headerName: "X-XSRF-TOKEN"})
      ),
    userServiceProvider, 
    wasteServiceProvider,
    companyServiceProvider,
    JwtHelperService, provideAnimationsAsync(),
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: 'outline'}}
  ]
};
