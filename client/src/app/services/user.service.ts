import { Injectable, PLATFORM_ID, Provider, inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, firstValueFrom, map } from 'rxjs';
import { isPlatformServer } from '@angular/common';
// import { JwtHelperService } from '@user0/angular-jwt';

const USER_API = 'https://localhost:3000/api/users/';

const httpUserOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Cache-Control': 'no-cache'}),
  withCredentials: true //this is required so that Angular returns the Cookies received from the server. The server sends cookies in Set-Cookie header.
  // Without this, Angular will ignore the Set-Cookie header
};


export class UserResult {
  status: string = "";
  message: string = "";
  data: Object|null = null;

  requestSucceed() {
    console.log(this.status);
    return ["VALID", "SUCCESS"].includes(this.status.toUpperCase());
  }

  requestFailed() {
    return ["ERROR", "CRITICAL"].includes(this.status.toUpperCase());
  }

  isNull() {
    return this.message.length == 0 && this.status.length == 0;
  }
}

@Injectable({
  providedIn: 'root',
})
export class BaseUserService {

  user: {[key: string]: any}|null = null;

  constructor(protected http: HttpClient) {
    console.log("BaseUserService created");
  }
  
  login(email: string, password: string): Observable<UserResult> {
    return new Observable<UserResult>((subscriber) => {
      subscriber.next({status: "", message: "", data: null} as UserResult);
      subscriber.complete()
    });
  }

  signup(name: string, email: string, password: string): Observable<UserResult> {
    return new Observable<UserResult>((subscriber) => {
      subscriber.next({status: "", message: "", data: null} as UserResult);
      subscriber.complete()
    }); 
  }

  logout(): Observable<UserResult> {
    return new Observable<UserResult>((subscriber) => {
      subscriber.next({status: "", message: "", data: null} as UserResult);
      subscriber.complete()
    });
  }

  isLogged(): Observable<boolean|null> {
    return new Observable<boolean|null>((subscriber) => {
      subscriber.next(null);
      subscriber.complete()
    });
  }

  async getUserData(key: string="") {
    if (this.user == null || key.length == 0)
      return this.user;
    return this.user[key];
  }
}


@Injectable({
  providedIn: 'root',
})
export class ClientUserService extends BaseUserService {

  override async getUserData(key: string="") {
    if (!this.user)
    {
      var logged_request = this.http.get<boolean|null>(USER_API + 'is_logged', httpUserOptions);
      var is_logged = await firstValueFrom(logged_request);
      if (!is_logged)
      {
        throw Error("Cannot fetch user data: not logged !");
      }
      var user_request = this.http.get<Object>(USER_API + 'user_data', httpUserOptions);
      var user = await firstValueFrom(user_request);
      this.user = user;
    }
    return await super.getUserData(key)
  }

  override login(email: string, password: string): Observable<UserResult> {
    var request = this.http.post<UserResult>(
      USER_API + 'login',
      {
        "email": email,
        "password": password,
      },
      httpUserOptions
    ).pipe(
      map(data => Object.assign(new UserResult(), data))
    );
    request.subscribe(result => this.user = result.data);
    return request;
  }

  override signup(name: string, email: string, password: string): Observable<UserResult> {
    return this.http.post<UserResult>(
      USER_API + 'signup',
      {
        "email": email,
        "password": password,
        "name": name
      },
      httpUserOptions
    ).pipe(
      map(data => Object.assign(new UserResult(), data))
    );
  }

  override logout(): Observable<UserResult> {
    return this.http.post(USER_API + 'logout', { }, httpUserOptions).pipe(
      map(data => Object.assign(new UserResult(), data))
    );
  }

  override isLogged(): Observable<boolean|null> {
    var request = this.http.get<boolean|null>(USER_API + 'is_logged', httpUserOptions);
    return request;
  }
}

var userService: null|BaseUserService = null;
export function userServiceFactory(platform_id: Object, http: HttpClient) {
  if (userService === null)
  {
    if(isPlatformServer(platform_id))
      userService = new BaseUserService(http);
    else
      userService = new ClientUserService(http);
  }
  return userService;
}

export const userServiceProvider: Provider = {
  provide: BaseUserService, useFactory: userServiceFactory,
  deps: [PLATFORM_ID, HttpClient]
}