import { Injectable, Provider } from '@angular/core'; 
import { HttpClient, HttpHeaders } from '@angular/common/http'; 
import { Observable, map } from 'rxjs';
import { request } from 'http';

const WASTES_API = 'https://localhost:3000/api/wastes/';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Cache-Control': 'no-cache'}),
    withCredentials: true //this is required so that Angular returns the Cookies received from the server. The server sends cookies in Set-Cookie header.
    // Without this, Angular will ignore the Set-Cookie header
};

export class WastesResult {
    status: string = "";
    message: string = "";
    data?: any;
  
    requestSucceed() {
      console.log(this.status);
      return ["VALID", "SUCCESS"].includes(this.status.toUpperCase());
    }
  
    requestFailed() {
      return ["ERROR", "CRITICAL"].includes(this.status.toUpperCase());
    }
  
    isNull() {
      return this.message.length == 0 && this.status.length == 0;
    }
  }
  
@Injectable({ 
    providedIn: 'root'
}) 
export class WastesService { 
    constructor(private http: HttpClient) { } 

    wastes_request(): Observable<WastesResult> { 
        return this.http.get<WastesResult>(WASTES_API + 'wastes', httpOptions).pipe(
            map(data => Object.assign(new WastesResult(), data))
        );
    } 
}

export const wasteServiceProvider: Provider = {
  provide: WastesService, useClass: WastesService,
  deps: [HttpClient]
}