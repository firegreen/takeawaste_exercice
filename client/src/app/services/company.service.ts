import { Injectable, Provider } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { share } from 'rxjs';


const USER_API = 'https://localhost:3000/api/company/';

const httpCompanyOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Cache-Control': 'no-cache'}),
  // Without this, Angular will ignore the Set-Cookie header
  params: new HttpParams()
};


export class CompanyResult {
  status: string = "";
  message: string = "";
  data: Object|null = null;

  requestSucceed() {
    console.log(this.status);
    return ["VALID", "SUCCESS"].includes(this.status.toUpperCase());
  }

  requestFailed() {
    return ["ERROR", "CRITICAL"].includes(this.status.toUpperCase());
  }

  isNull() {
    return this.message.length == 0 && this.status.length == 0;
  }
}


@Injectable({
    providedIn: 'root',
})
export class CompanyService {

    constructor(private http: HttpClient) { }

    get_all_companies_request(filter: string = "", limit = -1) {
        var options = Object.assign({}, httpCompanyOptions);
        options["params"].set("filter", filter);
        options["params"].set("limit", limit);
        return this.http.get<Array<string>>(USER_API + 'all_names', options).pipe(share());
    }

}

export const companyServiceProvider: Provider = {
    provide: CompanyService, deps: [HttpClient]
}
