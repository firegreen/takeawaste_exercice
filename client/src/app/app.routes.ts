import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CollectionComponent } from './collection/collection.component';
import { RegisterComponent } from './register/register.component';
import { CostComponent } from './cost/cost.component';
import { ProfileComponent } from './profile/profile.component';
import { TawPageNotFoundComponent } from './page_not_found/page_not_found.component';
import { inject } from '@angular/core';
import { check_authentified, check_not_authentified } from './helpers/authentification_check';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';

export const routes: Routes = [
    { path: 'dashboard', component: DashboardComponent, canActivate: [check_authentified]},
    { path: 'collection', component: CollectionComponent, canActivate: [check_authentified] },
    { path: 'register', component: RegisterComponent, canActivate: [check_authentified] },
    { path: 'cost', component: CostComponent, canActivate: [check_authentified] },
    { path: 'profile', component: ProfileComponent, canActivate: [check_authentified] },
    { path: 'signup', component: SignupComponent, canActivate: [check_not_authentified] },
    { path: 'login', component: LoginComponent, canActivate: [check_not_authentified] },
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    { path: '**', component: TawPageNotFoundComponent}
];
