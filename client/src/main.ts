import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
import { TawMainComponent } from './app/main/app.component';

bootstrapApplication(TawMainComponent, appConfig)
  .catch((err) => console.error(err));
