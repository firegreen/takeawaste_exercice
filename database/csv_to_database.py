import codecs
import psycopg2
from datetime import datetime

if __name__ == "__main__":
    columns = []
    data = []
    with open(r"take_a_waste.csv", mode="rb") as fp:
        columns = fp.readline().decode("utf8").strip(" \n").split(";")
        for i in range(len(columns)):
            try:
                columns[i] = eval(columns[i])
            except (SyntaxError, NameError, ValueError):
                pass
        print(columns)

        for line in fp:
            d = line.decode("utf8").strip(" \n\r").split(";")
            if len(d) != len(columns):
                print("Data invalid:", d)
                continue
            for i in range(len(d)):
                for _ in range(2):
                    if not d[i]:
                        d[i] = None
                    elif isinstance(d[i], str):
                        if d[i][0].isnumeric():
                            d[i] = d[i].replace(",", ".")
                        try:
                            d[i] = eval(d[i])
                        except (SyntaxError, NameError):
                            pass
            data.append(dict(zip(
                columns, d
            )))
    
    print(data[:10], data[-10:])

    connection = None
    try:
        # connecting to the PostgreSQL server
        pwd = input("Enter db password:")
        connection = psycopg2.connect(
            host="localhost", 
            port=5555, dbname="takeawaste", user="postgres", password=pwd
        )
    except (psycopg2.DatabaseError, Exception) as error:
        print(error)
        exit(2)
            
    companies = dict()
    treatments = dict()
    waste_types = dict()
    wastes = []
    cursor = connection.cursor()
    for waste in data:
        if waste["Company"] not in companies:
            cursor.execute(
                f"INSERT INTO COMPANY (name) VALUES (%s) "
                "RETURNING id",
                (waste["Company"], )
            )
            companies[waste["Company"]] = cursor.fetchone()[0]
        if waste["Libelle site de traitement"] not in companies:
            cursor.execute(
                f"INSERT INTO COMPANY (name) VALUES (%s) "
                "RETURNING id",
                (waste["Libelle site de traitement"] or '?', )
            )
            companies[waste["Libelle site de traitement"]] = cursor.fetchone()[0]
        if waste["Prestataire"] not in companies:
            cursor.execute(
                f"INSERT INTO COMPANY (name, is_provider) VALUES (%s, 1) "
                "RETURNING id",
                (waste["Prestataire"], )
            )
            companies[waste["Prestataire"]] = cursor.fetchone()[0]
        if waste["Code Traitement"] not in treatments:
            cursor.execute(
                f"INSERT INTO TREATMENT (label, code, category) "
                "VALUES (%s, %s, %s)"
                "RETURNING id",
                (
                    waste["Libellé traitement"] or '?', 
                    waste["Code Traitement"] or '?',
                    waste["Catégorie traitement"]
                )
            )
            treatments[waste["Code Traitement"]] = cursor.fetchone()[0]
        if waste["Nom dechet collecte"] not in waste_types:
            cursor.execute(
                f"INSERT INTO WASTE_TYPE (name) "
                "VALUES (%s)"
                "RETURNING id",
                (waste["Nom dechet collecte"] or '?',)
            )
            waste_types[waste["Nom dechet collecte"]] = cursor.fetchone()[0]


    for waste in data:
        if isinstance(waste['Opéré par Take a waste'], str):
            waste['Opéré par Take a waste'] = "Opéré par" in waste['Opéré par Take a waste']

        cursor.execute(
            f"INSERT INTO WASTE ("
            "date, type, provider_category, provider_id,"
            "units, volume, density, estimated_weight,"
            "actual_weight, selected_weight, unit_price, price, processed,"
            "company_id, treatment_site_id, treatment_id"
            ") "
            "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            (
                datetime.strptime(waste["Date"].split(" ", 1)[0], "%Y-%m-%d"),
                waste_types[waste["Nom dechet collecte"]],
                waste["Catégorie de prestation"], 
                companies[waste["Prestataire"]], 
                waste["Unités"],
                waste["Volume collecte (m3)"], 
                waste["Densite"],
                waste["Poids estimé"], 
                waste['Poids réel'], 
                waste['Poids retenu'], 
                waste['Prix unitaire'],
                waste['Total TTC'],
                waste['Opéré par Take a waste'], 
                companies[waste["Company"]],
                companies[waste["Libelle site de traitement"]],
                treatments[waste["Code Traitement"]],
            )
        )

    connection.commit()
    cursor.close()
    connection.close()
