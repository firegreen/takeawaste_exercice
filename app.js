import debug from 'debug'
import express from 'express';
import session from 'express-session';
import cookieParser from "cookie-parser";
import path from 'path';
import logger from 'morgan';
import cors from 'cors';
import api from './src/api/api.js'

import config from './src/helpers/config.js';
import { doubleCsrfProtection } from './src/helpers/crsf.js';

var app = express();


app.options(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Session settings
app.use(
  session({
    secret: 'carotte-46-juncko',
    resave: false,
    saveUninitialized: false,
    cookie: {maxAge: 1 * 24 * 60 * 60 * 1000} // 1 day
  })
);

app.use(
  cookieParser(
    'pineapple-12-poussifeu'
  )
)

app.use(
  doubleCsrfProtection
)

app.use("/public", cors())
app.use("/public", express.static(path.join(import.meta.dirname, 'public')));


app.use('/api', cors({credentials: true, origin: true}));
app.use('/api', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.redirect(config.get("client/url"));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  if (req.app.get('env') === 'development')
    console.log(err);
  // render the error page
  res.status(err.status || 500);
  res.json({status: "ERROR", message: err.toString()});
});

export default app;
